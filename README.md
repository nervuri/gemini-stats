# Gemini server statistics (2020-12)

Statistics on Gemini servers, gathered on 2020-12-22 (give or take a few days), based on the list of hosts found at gemini://gus.guru/known-hosts

Among other things, the repo contains:

* [data/certs/](data/certs/) - all certificates for servers listed by GUS
* [data/ip/ip-hosts](data/ip/ip-hosts) - ip & hostname pairs compatible with the OS's hosts file
* [cipher-suites.md](cipher-suites.md) - stats on TLSv1.2 cipher suites supported by Gemini servers

See [discussion on the Gemini mailing list](https://lists.orbitalfox.eu/archives/gemini/2020/004613.html).

To get complete results, `get-tls-supported-versions.sh` and `get-tls1_2-supported-ciphers.sh` need to be executed at least 2 times each, as connections might fail for various reasons.

## IP

**422 hosts** total

*   2 IPv6-only
* 326 IPv4-only
*  91 both IPv4 and IPv6
*   3 failed lookups

```
cat ip-stats | grep    '0 IPv4' | grep -v '0 IPv6' | wc -l
cat ip-stats | grep -v '0 IPv4' | grep    '0 IPv6' | wc -l
cat ip-stats | grep -v '0 IPv4' | grep -v '0 IPv6' | wc -l
cat ip-stats | grep    '0 IPv4' | grep    '0 IPv6' | wc -l
```

## TLS versions

* 26 hosts down (including 3 failed DNS lookups)
* **396 hosts** remaining

```
cat tls-versions | grep -v ' -tls' | wc -l
cat tls-versions | grep    ' -tls' | wc -l
```

Out of those **396**:

* all 396 support TLSv1.2
* 280 (71%) support TLSv1.3
* 116 (29%) do not support TLSv1.3
* 44 support TLSv1.1
* 43 support TLSv1.0

However, some hosts have multiple subdomains (flounder.online has 107!) on the same server, with identical TLS configs. Once we eliminate these duplicates, we have **258 hosts** remaining, out of which:

* 151 (59%) support TLSv1.3
* 107 (41%) do not support TLSv1.3
* 40 support TLSv1.1
* 39 support TLSv1.0

```
#file=tls-versions
file=tls-versions-no-dupes
cat $file | grep ' -tls' | wc -l
cat $file | grep ' -tls' | grep    tls1_2 | wc -l
cat $file | grep ' -tls' | grep    tls1_3 | wc -l
cat $file | grep ' -tls' | grep -v tls1_3 | wc -l
cat $file | grep "tls1_1" | wc -l
cat $file | grep "tls1$" | wc -l
```

## TLSv1.2 cipher suites

See [cipher-suites.md](cipher-suites.md).

## TLS Certificates

* 28 hosts without certs (including 26 hosts down)
* **394 hosts** remaining

Yes, there are 2 hosts (ex: walkaway.wiki) to which TLS connections can be initiated, but which don't send back any certs.

```
ls -lS certs | grep '  0'
```

Out of those **394**:

* 66 certs are signed by Let's Encrypt
* 35 pass OpenSSL validation
* 359 fail OpenSSL validation (not signed by a trusted CA, expired, etc)

```
cat cert-details | grep "Let's Encrypt Authority" | wc -l

for f in certs/*.pem; do
  openssl verify -untrusted $f $f
done | grep 'verification failed' | wc -l
```

* 347 serve a single cert
*  47 also serve an intermediate cert

```
for f in certs/*.pem; do
  printf "%s | " "$f"
  cat $f | grep '\-----BEGIN CERTIFICATE-----' | wc -l
done | grep -F '| 2' | wc -l
```

### Signature Algorithm

*   1 : ED25519 ( tdem.in )
*   1 : ecdsa-with-SHA512 ( freeside.wntrmute.net )
* 171 : ecdsa-with-SHA256 ( including flounder.online - 107 hosts )
*   3 : sha512WithRSAEncryption
* 217 : sha256WithRSAEncryption
*   1 : sha1WithRSAEncryption ( houston.coder.town )

### Public Key Algorithm

*   1 : ED25519
* 180 : id-ecPublicKey (ECDSA) ( including flounder.online - 107 hosts )
* 213 : rsaEncryption (RSA)

Yes, there are a few hosts that use RSA for Signature Algorithm and ECDSA for Public Key Algorithm. See https://letsencrypt.org/upcoming-features/#ecdsa-signing-support

### Key size

*   1 : ED25519 Public-Key: (256 bit)
* 164 : ECDSA Public-Key: (256 bit) ( including flounder.online - 107 hosts )
*  15 : ECDSA Public-Key: (384 bit)
*   1 : ECDSA Public-Key: (521 bit)
*   1 : RSA Public-Key: (1024 bit)
* 102 : RSA Public-Key: (2048 bit)
* 110 : RSA Public-Key: (4096 bit)

```
for f in certs/*.pem; do
  openssl x509 -noout -text -in $f
done > cert-details

cat cert-details | grep '        Signature Algorithm: sha256WithRSAEncryption' | wc -l
cat cert-details | grep 'Public Key Algorithm: ED25519' | wc -l
cat cert-details | grep 'Public-Key:'
```

### Expiration

#### Not Before

*   1 : Not Before 1975
*   6 : Not Before 2019
* 387 : Not Before 2020 ( including flounder.online - 107 hosts )

#### Not After

*   1 : Not After 2018
*   2 : Not After 2019
*  30 : Not After 2020
* 247 : Not After 2021 ( including flounder.online - 107 hosts )
*   2 : Not After 2022
*   6 : Not After 2023
*   2 : Not After 2024
*  56 : Not After 2025
*   1 : ...
*  38 : Not After 2030
*   6 : ...
*   3 : Not After 9999


```
cat cert-details | grep 'Not Before' | grep -v 2020
cat cert-details | grep 'Not After' | grep 2020 | wc -l
```

### Hostname mismatch

As of 2021-03-07, 8 out of 491 hosts (that's 1.6%) had a hostname mismatch - see [data/certs/hostname-mismatch](data/certs/hostname-mismatch).

Note that the CN for `fern91.com` and `tokeniser.uk` is set correctly; the mismatch is due to the Subject Alternative Name field, which takes priority.

## More Gemini stats

* [Lupa crawler](gemini://gemini.bortzmeyer.org/software/lupa/stats.gmi)
* [geminispace.info](gemini://geminispace.info/statistics)
* [GUS - Gemini Universal Search](gemini://gus.guru/statistics)
