#!/bin/sh

for f in ../data/certs/certs/*.pem; do

  # Verify cert validity
  #openssl verify -untrusted "$f" "$f"

  # Number of certs / chain
  #printf "%s | " "$f"
  #cat $f | grep '\-----BEGIN CERTIFICATE-----' | wc -l

  # View cert details
  openssl x509 -noout -text -in "$f"
  echo
  echo '======================================'
  echo

done
