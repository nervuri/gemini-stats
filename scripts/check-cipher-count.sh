#!/bin/sh

#file='../data/tls/tls-1_2-ciphers'
file='../data/tls/tls-1_2-ciphers-no-dupes'

# Go where this script is
cd "$(dirname "$0")" || exit

# Count occurrences of each cipher suite supported by TLS1.2
list=''
for c in $(openssl ciphers -s -tls1_2 'ALL:eNULL' | tr ':' ' '); do
  count=$(grep " $c" $file | wc -l)
  list="$list$count : $c\n"
done

# Sort list by count
echo $list | grep -v "^0 :" | sort -n | tac
