#!/bin/sh

# Go where this script is
cd "$(dirname "$0")" || exit

# Loop through all hosts
while read -r host
do

  printf "$host"

  # If $host contains ":", then it also contains the port
  if [ -z "${host##*:*}" ]; then
    port=$(echo "$host" | cut -d ':' -f 2)
    host=$(echo "$host" | cut -d ':' -f 1)
  else
    port="1965"  # default Gemini port
  fi

  #timeout 5 openssl s_client -verify_hostname "$host" -showcerts -servername "$host" -connect "$host:$port" </dev/null | openssl x509 -noout -text
  timeout 5 openssl s_client -verify_hostname "$host" -servername "$host" -connect "$host:$port" </dev/null 2>&1\
    | grep -q 'Hostname mismatch' && printf " | HOSTNAME MISMATCH"
  printf "\\n"

  sleep 0.5

done < hosts
