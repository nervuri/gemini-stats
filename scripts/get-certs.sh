#!/bin/sh

# Go where this script is
cd "$(dirname "$0")" || exit

mkdir certs

# Loop through all hosts, checking what TLS versions are supported
while read -r host
do
	echo "$host"

	# If $host contains ":", then it also contains the port
	if [ -z "${host##*:*}" ]; then
		port=$(echo "$host" | cut -d ':' -f 2)
		host=$(echo "$host" | cut -d ':' -f 1)
	else
		port="1965"  # default Gemini port
	fi

	timeout 3 openssl s_client -showcerts -servername "$host" -connect "$host:$port" </dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'\
		> "certs/$host.pem"

	sleep 0.5

done < hosts
