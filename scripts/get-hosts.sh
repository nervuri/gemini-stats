#!/bin/sh

#curl -s "https://portal.mozz.us/gemini/gus.guru/known-hosts?raw=1" | grep "gemini://" | cut -d ' ' -f 3 > hosts

# geminispace.info is more up-to-date.
# Exclude flounder.online subdomains, they skew the stats too much.
curl -s "https://portal.mozz.us/gemini/geminispace.info/known-hosts?raw=1" | grep "gemini://" | cut -d ' ' -f 3 | grep -v '.flounder.online' > hosts
