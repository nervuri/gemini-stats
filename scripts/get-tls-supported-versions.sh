#!/bin/sh

# Go where this script is
cd "$(dirname "$0")" || exit

# Check TLS versions
tls() {
	version=$1
	hostname=$2

	# If $hostname contains ":", then it also contains the port
	if [ -z "${hostname##*:*}" ]; then
		port=$(echo "$hostname" | cut -d ':' -f 2)
		hostname=$(echo "$hostname" | cut -d ':' -f 1)
	else
		port="1965"  # default Gemini port
	fi

	timeout 5 openssl s_client "$version" -servername "$hostname" -connect "$hostname:$port" </dev/null >/dev/null 2>&1\
		&& printf " %s" "$version"
	sleep 0.5
}

# Loop through all hosts, checking what TLS versions are supported
while read -r host
do
	printf "%s |" "$host"
	tls -tls1_3 "$host"
	tls -tls1_2 "$host"
	tls -tls1_1 "$host"
	tls -tls1   "$host"
	printf "\\n"
done < hosts
