#!/bin/sh

# Go where this script is
cd "$(dirname "$0")" || exit

while read -r host
do
	# If $host contains ":", then it also contains the port
	if [ -z "${host##*:*}" ]; then
		port=$(echo "$host" | cut -d ':' -f 2)
		host=$(echo "$host" | cut -d ':' -f 1)
	else
		port="1965"  # default Gemini port
	fi

	# Check if TLS 1.2 is supported
	timeout 3 openssl s_client -tls1_2 -servername "$host" -connect "$host:$port" </dev/null >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		printf "%s |" "$host"
		# https://www.ise.io/using-openssl-determine-ciphers-enabled-server/
		for c in $(openssl ciphers -s -tls1_2 'ALL:eNULL' | tr ':' ' '); do
			timeout 3 openssl s_client -tls1_2 -cipher "$c" -servername "$host" -connect "$host:$port" </dev/null >/dev/null 2>&1\
				&& printf " %s" "$c"
			sleep 0.1
		done
		printf "\\n"
	fi
	sleep 0.1
done < hosts
