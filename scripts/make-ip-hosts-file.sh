#!/bin/sh

# Run like this:
# ./build-ip-hosts-file.sh >ip-hosts 2>ip-stats

# Go where this script is
cd "$(dirname "$0")" || exit

# Loop through all hosts
while read -r host
do
	# if $host contains ":", then it also contains the port, which we don't need
	if [ -z "${host##*:*}" ]; then
		host=$(echo "$host" | cut -d ':' -f 1)
	fi

	# look up the host
	ips=$(host "$host")

	# extract IPs and append $host to each row
	ipv4=$(echo "$ips" | grep 'has address' | cut -d ' ' -f 4 | sed -e "s/$/ $host/")
	ipv6=$(echo "$ips" | grep 'has IPv6 address' | cut -d ' ' -f 5 | sed -e "s/$/ $host/")

	ipv4_count=0
	ipv6_count=0

	# hosts file @ stdout
	if [ -n "$ipv4" ]; then
		echo "$ipv4"
		ipv4_count=$(echo "$ipv4" | wc -l)
	fi
	if [ -n "$ipv6" ]; then
		echo "$ipv6"
		ipv6_count=$(echo "$ipv6" | wc -l)
	fi

	# stats file @ stderr
	echo "$host | $ipv4_count IPv4 | $ipv6_count IPv6" >&2

	sleep 0.3
done < hosts
